import React, { Fragment } from 'react';
import './App.css';
import { BrowserRouter, Link, Switch, Route } from 'react-router-dom';
import Header from './Components/Header';
import Footer from './Components/Footer';
import Login from './Components/Login';
import Home from './Components/Home';
import MetodePembayaran from './Components/MetodePembayaran';
import FormInputPelanggan from './Components/FormInputPelanggan';
import DataPelanggan from './Components/DataPelanggan';

function App() {
  return (
    <BrowserRouter>
      <Fragment>
        <Switch>


          <Route path="/" exact component={Home} />
          <Route path="/metodePembayaran" component={MetodePembayaran} />
          <Route path="/formInputPelanggan" component={FormInputPelanggan} />
          <Route path="/dataPelanggan" component={DataPelanggan} />
          <Route path="/login" component={Login} />
        </Switch>
      </Fragment>
    </BrowserRouter>
  );
}

export default App;
