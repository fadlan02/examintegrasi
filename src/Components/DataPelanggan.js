import React, { Component, Fragment } from 'react';
import Axios from 'axios';
import Header from './Header';
import Footer from './Footer';
class DataPelanggan extends Component {
    state = {
        pelangganDatas: [],
        idPelanggan: '',
        kodePemesanan: '',
        namaPelanggan: '',
        berat: '',
        tanggal: ''
    }

    componentDidMount() {
        Axios.get(`http://localhost:8787/pelanggans`)
            .then(
                res => {
                    const pelangganDatas = res.data;
                    this.setState({
                        pelangganDatas
                    })
                }
            )
    }

    handleDelete = (id) => {
        console.log(id);
        Axios.delete('http://localhost:8787/deletePelanggan/' + id)
            .then(res => {
                const pelangganDatas = res.data;
                this.setState({
                    pelangganDatas
                });
            })
        window.location.reload();
    }

    render() {
        return (
            <Fragment>
                <Header />
                {/* ========================== Modal ======================== */}
                <div className="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="exampleModalLabel">Modal title</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <label>kodePemesanan</label><br />
                                <input type="text" placeholder="Kode pemesanan" name="kodePemesanan" /><br />
                                <label>namaPelanggan</label><br />
                                <input type="text" placeholder="Nama Pelanggan" name="namaPelanggan" /><br />
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="button" className="btn btn-primary">Save changes</button>
                            </div>
                        </div>
                    </div>
                </div>
                {/* ==========================End Modal ======================== */}
                <h3 className="title">Data Pelanggan</h3>
                <input type="text" name="search" placeholder="" />
                <button className="btn btn-outline-success">Cari</button>
                <table width="90%" className="table">
                    <thead>
                        <tr>
                            <th scope="cols">Kode Pemesanan</th>
                            <th scope="cols">Nama Pelanggan</th>
                            <th scope="cols">berat</th>
                            <th scope="cols">Tanggal</th>
                            <th scope="cols">Setting</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.pelangganDatas.map(pelangganDatas =>
                            <tr key="pelangganDatas.idPelanggan">
                                <td>{pelangganDatas.kodePemesanan}</td>
                                <td>{pelangganDatas.namaPelanggan}</td>
                                <td>{pelangganDatas.berat}</td>
                                <td>{pelangganDatas.tanggal}</td>
                                <td><button className="btn btn-warning" data-toggle="modal" data-target="#exampleModal" style={{ marginRight: "2px" }}><i className="fas fa-edit"></i>Edit</button>
                                    <button className="btn btn-danger" onClick={() => this.handleDelete(pelangganDatas.idPelanggan)}><i className="fas fa-trash"></i> Hapus</button>
                                </td>

                            </tr>
                        )}
                    </tbody>
                </table>
                <Footer />
            </Fragment>
        );
    }
}

export default DataPelanggan;