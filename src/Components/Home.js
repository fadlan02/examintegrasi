import React, { Component, Fragment } from 'react';
import Header from './Header';
import { Link } from 'react-router-dom';
class Home extends Component {
    state = {}
    render() {
        return (
            <Fragment>
                <Header />
                <div className="card menuHome" style={{ width: "18rem", margin: "20px" }}>
                    <div className="card-body">
                        <h5 className="card-title">Pemesanan</h5>
                        <h6 className="card-subtitle mb-2 text-muted">menu pemesanan</h6>
                        <p className="card-text">klik disini untuk ke memesan.</p>
                        <Link to="/metodePembayaran">
                            <p className="card-link">Klik disini</p>
                        </Link>
                    </div>
                </div>
                <div className="card menuHome" style={{ width: "18rem", margin: "20px" }}>
                    <div className="card-body">
                        <h5 className="card-title">Pengambilan</h5>
                        <h6 className="card-subtitle mb-2 text-muted">menu Pengambilan</h6>
                        <p className="card-text">klik disini untuk ambil barang.</p>
                        <Link to="">
                            <p className="card-link">Klik disini</p>
                        </Link>
                    </div>
                </div>
                <footer style={{ position: "absolute", bottom: "0" }}><p>copyright &copy; 2020 by fadlan</p></footer>
            </Fragment>
        );
    }
}

export default Home;