import React, { Component, Fragment } from 'react';
import Header from './Header';
import Footer from './Footer';
import Axios from 'axios';
class FormInputPelanggan extends Component {
    state = {
        kodePemesanan: '',
        namaPelanggan: '',
        berat: '',
        tanggal: ''
    }

    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        });
        console.log(this.state.kodePemesanan);
    }

    handleSubmit = (e) => {
        e.preventDefault()
        Axios.post('http://localhost:8787/addPelanggan', this.state)
            .then(res => {
                alert("Data Berhasil Di Tambah ! ")
            })
        this.props.history.push("/dataPelanggan")

    }

    render() {
        return (
            <Fragment>
                <Header />
                <h3 className="title">Form Input Pelanggan</h3>
                <div className="container">
                    <div className="row">
                        <div className="col-sm-6">
                            <label>Kode Pemesanan</label><br />
                            <input type="text" className="form-control" onChange={this.handleChange} placeholder="kode pemesanan" name="kodePemesanan"></input><br />
                            <label>Nama Pelanggan</label><br />
                            <input type="text" className="form-control" placeholder="Nama Pelanggan" name="namaPelanggan" onChange={this.handleChange}></input><br />
                            <label>Berat</label>
                            <input type="number" name="berat" onChange={this.handleChange} className="form-control" placeholder="berat Berdasarkan satuan Kg"></input><br />
                            <label>Tanggal</label>
                            <input type="date" name="tanggal" className="form-control" placeholder="tanggal" onChange={this.handleChange} ></input><br />
                            <button className="btn btn-primary" onClick={this.handleSubmit}> Kirim </button>
                        </div>
                    </div>
                </div>
                <Footer />


            </Fragment>
        );
    }
}

export default FormInputPelanggan;