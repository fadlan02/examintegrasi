import React, { Component, Fragment } from 'react';
import '../App.css'
import { Link } from 'react-router-dom';
class Header extends Component {
    state = {}
    render() {
        return (
            <Fragment>
                <nav>
                    <ul>
                        <Link to="/">
                            <li>Home</li>
                        </Link>
                        <Link to="/dataPelanggan">
                            <li>Data Pelanggan</li>
                        </Link>
                        <Link to="/history">
                            <li>History</li>
                        </Link>
                    </ul>
                </nav>
            </Fragment>
        );
    }
}

export default Header;