import React, { Component, Fragment } from 'react';
import { Link } from 'react-router-dom';
class Login extends Component {
    state = {}
    render() {
        return (
            <Fragment>

                <div className="card-login">
                    <h2 className="title-card">Login</h2>
                    <hr />
                    <i className="fas fa-user"></i><input type="text" name="username" placeholder="username" /><br />
                    <i className="fas fa-lock"></i><input type="password" name="password" placeholder="password" /><br />
                    <Link to="/">
                        <button className="btn btn-primary">Login</button>
                    </Link>
                </div>


            </Fragment >
        );
    }
}

export default Login;