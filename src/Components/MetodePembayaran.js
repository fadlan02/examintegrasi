import React, { Component, Fragment } from 'react';
import Header from './Header';
import Footer from './Footer';
import { Link } from 'react-router-dom';
class MetodePembayaran extends Component {
    state = {}
    render() {
        return (
            <Fragment>
                <Header />
                <div className="card-login">
                    <h1 className="title-card">Metode Pembayaran</h1>
                    <Link to="/formInputPelanggan">
                        <button className="btn btn-info" style={{ margin: "0 auto" }}>Langsung Bayar</button>
                    </Link>
                    <Link to="/formInputPelanggan">
                        <button className="btn btn-primary">Bayar Nanti</button>
                    </Link>
                </div>
                <Footer />

            </Fragment>
        );
    }
}

export default MetodePembayaran;